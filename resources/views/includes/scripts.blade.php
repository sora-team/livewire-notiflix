<script>
    Livewire.on('livewire-notiflix:notify', params => {
        const detail = params[0];
        callFn(detail.fnName, [
            detail.message,
            (detail.onNotifyClick)
                ? (() => {
                    Livewire.dispatch(detail.onNotifyClick)
                })
                : undefined,
            detail.options
        ]);
    });

    Livewire.on('livewire-notiflix:report', params => {
        const detail = params[0];
        callFn(detail.fnName, [
            detail.title,
            detail.message,
            detail.buttonText,
            (detail.onReportClick)
                ? (() => {
                    Livewire.dispatch(detail.onReportClick)
                })
                : undefined,
            detail.options
        ]);
    });

    Livewire.on('livewire-notiflix:block', params => {
        const detail = params[0];
        callFn(detail.fnName, [
            detail.element,
            detail.message,
            detail.options
        ]);
    });

    Livewire.on('livewire-notiflix:unblock', params => {
        const detail = params[0];
        callFn(detail.fnName, [
            detail.element,
            detail.delay
        ]);
    });

    Livewire.on('livewire-notiflix:loading', params => {
        const detail = params[0];
        callFn(detail.fnName, [
            detail.message,
            detail.options
        ]);
    });

    Livewire.on('livewire-notiflix:loaded', params => {
        const detail = params[0];
        callFn(detail.fnName, [
            detail.delay
        ]);
    });

    Livewire.on('livewire-notiflix:confirming', params => {
        Livewire.on(params[0], e => {
            const detail = e[0];
            callFn(detail.fnName, [
                detail.title,
                detail.message,
                detail.confirmButtonText,
                detail.cancelButtonText,
                function(){
                    Livewire.dispatch(detail.onConfirmed, { params: detail.options["params"] })
                },
                function(){
                    const cancelCallback = detail.onCancelled;
                    if (!cancelCallback) { return; }
                    Livewire.dispatch(cancelCallback)
                },
                detail.options
            ]);
        });
    });

    Livewire.on('livewire-notiflix:asking', params => {
        Livewire.on(params[0], e => {
            const detail = e[0];
            callFn(detail.fnName, [
                detail.title,
                detail.question,
                detail.answer,
                detail.answerButtonText,
                detail.cancelButtonText,
                function(){
                    Livewire.dispatch(detail.onAskConfirmed,{ params: detail.options["params"] })
                },
                function(){
                    const cancelCallback = detail.onAskCancelled;
                    if (!cancelCallback) { return; }
                    Livewire.dispatch(cancelCallback)
                },
                detail.options
            ]);
        });
    });
</script>
