<?php

namespace Sorateam\LivewireNotiflix;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Sorateam\LivewireNotiflix\LivewireNotiflixManager
 */
class LivewireNotiflix extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {        
        return 'livewire-notiflix';
    }
}