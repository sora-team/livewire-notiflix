<?php

namespace Sorateam\LivewireNotiflix;

class LivewireNotiflixBladeDirectives
{
    public static function livewireNotiflixScripts()
    {
        return '{!! \Sorateam\LivewireNotiflix\LivewireNotiflix::scripts() !!}';
    }
}