<?php

namespace Sorateam\LivewireNotiflix;

use Str;
use Exception;
use Livewire\Component;

class LivewireNotiflixMacros
{
    public static function registerMacros()
    {
        $instance = new static;

        $instance->registerNotifyMacro();
        $instance->registerReportMacro();
        $instance->registerBlockMacro();
        $instance->registerUnBlockMacro();
        $instance->registerConfirmMacro();
        $instance->registerAskMacro();
        $instance->registerLoadingMacro();
        $instance->registerLoadedMacro();
    }

    protected function registerNotifyMacro()
    {
        // Register Notify Macro
        Component::macro('notify', function ($type = 'success', $message = 'Message', $options = [], $useCallback = false) {
            $options = array_merge(config('livewire-notiflix.notify') ?? [], $options);

            $this->dispatch('livewire-notiflix:notify', [
                'fnName' => LivewireNotiflix::getNotiflixFnName('Notify', $type),
                'message' => $message,
                'options' => collect($options)->except([
                    'onNotifyClick',
                ])->toArray(),
                'onNotifyClick' => $useCallback ? $options['onNotifyClick'] : null,
            ]);
        });
    }

    protected function registerReportMacro()
    {
        // Register Report Macro
        Component::macro('report', function ($type = 'success', $title = 'Title', $message = 'Message', $buttonText = 'Okay', $options = [], $useCallback = false) {
            $options = array_merge(config('livewire-notiflix.report') ?? [], $options);

            $this->dispatch('livewire-notiflix:report', [
                'fnName' => LivewireNotiflix::getNotiflixFnName('Report', $type),
                'title' => $title,
                'message' => $message,
                'buttonText' => $buttonText,
                'options' => collect($options)->except([
                    'onReportClick',
                ])->toArray(),
                'onReportClick' => $useCallback ? $options['onReportClick'] : null,
            ]);
        });
    }

    protected function registerBlockMacro()
    {
        // Register Block Macro
        Component::macro('block', function ($element, $type = 'standard', $message = null, $options = []) {
            $options = array_merge(config('livewire-notiflix.block') ?? [], $options);

            $this->dispatch('livewire-notiflix:block', [
                'element' => $element,
                'fnName' => LivewireNotiflix::getNotiflixFnName('Block', $type),
                'message' => $message,
                'options' => $options,
            ]);
        });
    }

    protected function registerUnBlockMacro()
    {
        // Register UnBlock Macro
        Component::macro('unblock', function ($element, $delay = 0) {
            $this->dispatch('livewire-notiflix:unblock', [
                'element' => $element,
                'fnName' => LivewireNotiflix::getNotiflixFnName('Block', 'remove'),
                'delay' => $delay,
            ]);
        });
    }

    protected function registerLoadingMacro()
    {
        // Register Loading Macro
        Component::macro('loading', function ($type = 'standard', $message = null, $options = []) {
            $options = array_merge(config('livewire-notiflix.loading') ?? [], $options);

            $this->dispatch('livewire-notiflix:loading', [
                'fnName' => LivewireNotiflix::getNotiflixFnName('Loading', $type),
                'message' => $message,
                'options' => $options,
            ]);
        });
    }

    protected function registerLoadedMacro()
    {
        // Register Loaded Macro
        Component::macro('loaded', function ($delay = 0) {
            $this->dispatch('livewire-notiflix:loaded', [
                'fnName' => LivewireNotiflix::getNotiflixFnName('Loading', 'remove'),
                'delay' => $delay,
            ]);
        });
    }

    protected function registerConfirmMacro()
    {
        //Register Confirm Macro
        Component::macro('confirm', function ($title = 'Title', $message = 'Message', $confirmButtonText = 'Confirm', $cancelButtonText = 'Cancel', $options = []) {
            $options = array_merge(config('livewire-notiflix.confirm') ?? [], $options);

            $identifier = Str::uuid()->toString();

            $this->dispatch('livewire-notiflix:confirming', $identifier);

            $this->dispatch($identifier, [
                'fnName' => LivewireNotiflix::getNotiflixFnName('Confirm', 'show'),
                'title' => $title,
                'message' => $message,
                'confirmButtonText' => $confirmButtonText,
                'cancelButtonText' => $cancelButtonText,
                'options' => collect($options)->except([
                    'onConfirmed',
                    'onCancelled'
                ])->toArray(),
                'onConfirmed' => $options['onConfirmed'],
                'onCancelled' => $options['onCancelled'] ?? null
            ]);
        });
    }

    protected function registerAskMacro()
    {
        //Register Ask Macro
        Component::macro('ask', function ($title = 'Title', $question = 'Question', $answer = 'Answer', $answerButtonText = 'Answer', $cancelButtonText = 'Cancel', $options = []) {
            $options = array_merge(config('livewire-notiflix.ask') ?? [], $options);

            $identifier = Str::uuid()->toString();

            $this->dispatch('livewire-notiflix:asking', $identifier);

            $this->dispatch($identifier, [
                'fnName' => LivewireNotiflix::getNotiflixFnName('Confirm', 'ask'),
                'title' => $title,
                'question' => $question,
                'answer' => $answer,
                'answerButtonText' => $answerButtonText,
                'cancelButtonText' => $cancelButtonText,
                'options' => collect($options)->except([
                    'onAskConfirmed',
                    'onAskCancelled'
                ])->toArray(),
                'onAskConfirmed' => $options['onAskConfirmed'],
                'onAskCancelled' => $options['onAskCancelled'] ?? null
            ]);
        });
    }
}
